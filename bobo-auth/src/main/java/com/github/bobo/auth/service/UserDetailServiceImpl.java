package com.github.bobo.auth.service;


import com.github.bobo.auth.util.UserDetailsImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailService")
public class UserDetailServiceImpl implements UserDetailsService {

    @Override
    public UserDetailsImpl loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }
}
