package com.github.bobo.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoboCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoboCommonApplication.class, args);
    }
}
